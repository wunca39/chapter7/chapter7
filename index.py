from flask import Flask,render_template
from db_information import *
from flask_googlemaps import GoogleMaps
from flask_googlemaps import Map
# from print_linenotify import *
from datetime import datetime
import pymysql

app = Flask(__name__)
GoogleMaps(app)

# --------------------------  Chapter 9 -------------------------------------
def check9_DeviceStatus():
    return True
# --------------------------  End Chapter 9 ---------------------------------

# --------------------------  Chapter 10 ------------------------------------
def check10_Location():
    return True
# --------------------------  End Chapter 10 --------------------------------

# --------------------------  Chapter 11 ------------------------------------
def check11_WebStatus():
    return True
# --------------------------  End Chapter 11 --------------------------------

# --------------------------  Chapter 12 ------------------------------------
def check12_DNSResponse():
    return True
# --------------------------  End Chapter 12 --------------------------------

# --------------------------  Chapter 13 ------------------------------------
def check13_SNMPStatus():
    return True
# --------------------------  End Chapter 13 --------------------------------

# --------------------------  Chapter 14 ------------------------------------
def check14_APClient():
    return True
# --------------------------  End Chapter 14 --------------------------------

# --------------------------  Chapter 15 ------------------------------------
def check15_APLocation():
    return True
# --------------------------  End Chapter 15 --------------------------------

# --------------------------  Chapter 16 ------------------------------------
def check16_RadiusAuthen():
    return True
# --------------------------  End Chapter 16 --------------------------------

# --------------------------  Chapter 17 ------------------------------------
def check17_ServerService():
    return True
# --------------------------  End Chapter 17 --------------------------------

# --------------------------  Chapter 18 ------------------------------------
def check18_ServerStatus():
    return True
# --------------------------  End Chapter 18 --------------------------------

# --------------------------  Chapter 18 ------------------------------------
def check21_LineNotify():
    return True
# --------------------------  End Chapter 18 --------------------------------

@app.route('/')

def showData():
    ch9 = check9_DeviceStatus()             # Chapter 9
    ch10 = check10_Location()               # Chapter 10
    ch11 = check11_WebStatus()              # Chapter 11
    ch12 = check12_DNSResponse()            # Chapter 12
    ch13 = check13_SNMPStatus()             # Chapter 13
    ch14 = check14_APClient()               # Chapter 14
    ch15 = check15_APLocation()             # Chapter 15
    ch16 = check16_RadiusAuthen()           # Chapter 16
    ch17 = check17_ServerService()          # Chapter 17
    ch18 = check18_ServerStatus()           # Chapter 18
    check21_LineNotify()                    # Chapter 21
    return render_template('index.html')

if __name__ == '__main__':
    app.run(debug=True)
